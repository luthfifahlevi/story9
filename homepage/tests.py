from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import index, vote
from .models import Buku
from .apps import HomepageConfig
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time

class UnitTest(TestCase):
    def test_app(self):
        self.assertEqual(HomepageConfig.name, "homepage")

    def test_index_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Search Your Books!")
    def test_index_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    def test_vote_func(self):
        found = resolve('/vote/')
        self.assertEqual(found.func, vote)

    def test_buku_models(self):
        Buku.objects.create(
            id_buku='q1LVDAAAQBAJ', 
            gambar='http://books.google.com/books/content?id=q1LVDAAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api',
            judul='Haha And Hehe Have Fun : All Set To Read')
        count = Buku.objects.all().count()
        self.assertEqual(count, 1)
    def test_model_can_print(self):
        message = Buku.objects.create(
            id_buku='q1LVDAAAQBAJ', 
            gambar='http://books.google.com/books/content?id=q1LVDAAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api',
            judul='Haha And Hehe Have Fun : All Set To Read')
        self.assertEqual(message.__str__(), "Haha And Hehe Have Fun : All Set To Read")


class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument("--window-size=1920,1080")
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path='./chromedriver.log'
        service_args=['--verbose']
        self.browser = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
        time.sleep(1)

    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def test_searching_and_like(self):
        selenium = self.browser
        selenium.get(self.live_server_url+'/')

        #mengecek dapat dilakukan searching
        input_box = selenium.find_element_by_class_name("inputme")
        button_search = selenium.find_element_by_class_name("btn-success")
        input_box.send_keys("Hehe")
        button_search.click()
        time.sleep(3)
        self.assertIn("Hehe", selenium.page_source)
        self.assertIn("0", selenium.page_source)

        #mengecek dapat dilakukan like
        button_vote = selenium.find_element_by_class_name("btn-primary")
        jumlah_vote = selenium.find_element_by_class_name("count")
        before = jumlah_vote.text
        button_vote.click()
        time.sleep(3)
        after = jumlah_vote.text
        self.assertNotEquals(before, after)
        time.sleep(3)
    
    def test_modal(self):
        selenium = self.browser
        selenium.get(self.live_server_url+'/')

        #mengecek bahwa modal masih kosong karena belum di like
        button_open_modal = selenium.find_element_by_id("modal")
        button_open_modal.click()
        time.sleep(3)
        self.assertIn("Buku Terpopuler", selenium.page_source)
        self.assertNotIn("Jumlah Vote: ", selenium.page_source)
        button_close_modal = selenium.find_element_by_class_name("btn-secondary")
        button_close_modal.click()
        time.sleep(3)
        
        #melakukan searching dan like
        input_box = selenium.find_element_by_class_name("inputme")
        button_search = selenium.find_element_by_id("cari")
        input_box.send_keys("Hehe")
        button_search.click()
        time.sleep(3)
        button_vote = selenium.find_element_by_class_name("btn-primary")
        button_vote.click()
        time.sleep(3)

        #mengecek isi modal bahwa seharusnya muncul
        button_open_modal = selenium.find_element_by_id("modal")
        button_open_modal.click()
        time.sleep(3)
        self.assertIn("Buku Terpopuler", selenium.page_source)
        self.assertIn("Jumlah Vote: 1", selenium.page_source)