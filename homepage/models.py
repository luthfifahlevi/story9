from django.db import models

# Create your models here.
class Buku(models.Model):
    id_buku = models.CharField(max_length=99, unique=True)
    gambar = models.CharField(max_length=250, default="")
    judul = models.CharField(max_length=250)
    jumlah_vote = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.judul