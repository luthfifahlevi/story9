var count;
var items = [];
$(function() {
    $(".btn-success").click(function(){
        searching($(':text').val())
    })

    $(":text").keypress(function(event) {
        if (event.which == 13) {
        $(".btn-success").click();
        }
    })
    
    function searching(q) {
        console.log(q);
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + q,
            type: "GET",
            success: function(result) {
                $("tbody").empty()
                items = result.items;
                count = Object.keys(items).length;
                for(var i = 0; i < count; i++){
                    try{
                        var item = items[i]["volumeInfo"]
                        $("tbody").append($("<tr></tr>")
                        .append($("<td></td>").html("<img src='" + item.imageLinks.smallThumbnail + "'></img>"))
                        .append($("<td></td>").html(item.title))
                        .append($("<td></td>").html(item.publisher + " (" + item.publishedDate + ")"))
                        .append($("<td></td>").html(item.averageRating))
                        .append($("<td></td>").html("<button type='button' class='btn btn-primary id" + i +
                                                    "' onclick='vote("+ i +")'>Like!</button><br>" +
                                                    "<p class='count idx" + i + "'>0</p>"))
                        )}catch(error){
                            console.log(error)
                        }
                }
            }
        })
    }   

})

function vote(i){
    var hasil_search = JSON.stringify(items[i]); 
    console.log(hasil_search);
    $.ajax({
        url: "/vote/",
        type: "POST",
        data: hasil_search,
        success: function(result){
            var liked = result.jumlah_vote;
            $(".idx"+i).html(liked);
        }
    })  
}

function modal() {
    $.ajax({
        url: "/",
        type: "POST",
        success: function(result){
            var buku = JSON.parse(result);
            console.log(buku)
            $('.modal-body').empty()
            for (i = 0; i < buku.length; i++) {
                $('.modal-body')
                .append($("<div></div>").addClass("card mb-3").attr("max-width", "540px")
                .append($("<div></div>").addClass("row no-gutters")
                .append($("<div></div>").addClass("col-md-4")
                .html("<img src='"+ buku[i].gambar +"' class='card-img' alt='Sampul Buku'>"))
                .append($("<div></div>").addClass("col-md-8")
                .append($("<div></div>").addClass("card-body")
                .append($("<h5></h5>").addClass("card-title").text(buku[i].judul))
                .append($("<p></p>").addClass("card-text").text("Jumlah Vote: " + buku[i].jumlah_vote))))))
            }
        }
    });
}