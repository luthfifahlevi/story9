from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Buku
import json



# Create your views here.
@csrf_exempt
def index(request):
    if request.method == "POST":
        books = list(Buku.objects.order_by("-jumlah_vote").values()[:5])
        return HttpResponse(json.dumps(books))
    else:
        return render(request, 'index.html')

@csrf_exempt
def vote(request):
    data = json.loads(request.body)
    if request.method == "POST":
        id_buku = data['id']
        judul_buku = data['volumeInfo']['title']
        sampul_buku =  data['volumeInfo']['imageLinks']['smallThumbnail']
        #liked = buku yang di like
        liked = Buku.objects.get_or_create(id_buku = id_buku, gambar=sampul_buku, judul=judul_buku)[0]
        liked.jumlah_vote += 1
        liked.save()
        return JsonResponse({'jumlah_vote' : liked.jumlah_vote})
